print()
print("################################")
print("Starting testJSONfiles.py")
print("I will check all .json files in this folder")
print()

import json

def testJSONfile( fileName ):
    print()
    print("%s -> test started"%fileName)
    with open(fileName) as json_file:
        data = json.load(json_file)
        if "Error" in fileName:
            raise Exception("ERROR IN testJSONfile %s"%fileName)
        pass
        print("%s -> test passed"%fileName)
    return True

from os import listdir
from os.path import isfile, join
current_path = "."
onlyfiles = [f for f in listdir(current_path) if isfile(join(current_path, f)) and ".json" in f]

for fileName in onlyfiles:
    testJSONfile(fileName)

print()
print("testJSONfiles.py is DONE without errors.")
print("################################")
print()
